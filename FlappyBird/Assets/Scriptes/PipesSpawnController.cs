﻿using System.Collections.Generic;
using UnityEngine;
using Zenject;
using GameConfiguration;
using PlayableElements;

namespace Controllers
{
    public class PipesSpawnController : MonoBehaviour
    {
        [Inject] private GameConfig _gameConfig;
        [Inject] private GameController _gameController;
        [Inject] private SignalBus _signalBus;
        private List<GameObject> pipesPool = new List<GameObject>();
        private float _spawnTimer = 0;

        private void Start()
        {
            for (int i = 0; i < _gameConfig.pipesCount; i++)
            {
                var tempPipe = Instantiate(_gameConfig.pipesPrefab, transform);
                tempPipe.SetActive(false);
                pipesPool.Add(tempPipe);
            }

            _signalBus.Subscribe<OnStartSignal>(ResetPool);
        }

        private void Update()
        {
            if (_gameController.GameState == GameState.Play)
            {
                _spawnTimer += Time.deltaTime;
                if (_spawnTimer > _gameConfig.spawnRate)
                {
                    _spawnTimer = 0;
                    GetPipeFromPool();
                }
            }
        }

        private void GetPipeFromPool()
        {
            for (int i = 0; i < pipesPool.Count; i++)
            {
                if (!pipesPool[i].activeSelf)
                {
                    var tempPipe = pipesPool[i];
                    tempPipe.transform.position = new Vector3(_gameConfig.pipesStartPos.x,
                        Random.Range(_gameConfig.minYSpawn, _gameConfig.maxYSpawn), _gameConfig.pipesStartPos.z);
                    tempPipe.SetActive(true);
                    tempPipe.GetComponent<PipeController>().OnSpawned();

                    break;
                }
            }
        }

        private void ResetPool()
        {
            for (int i = 0; i < pipesPool.Count; i++)
            {
                if (pipesPool[i].activeSelf)
                {
                    var tempPipe = pipesPool[i];
                    tempPipe.transform.position = Vector3.zero;
                    tempPipe.SetActive(false);
                }
            }
        }
    }
}


