﻿using UnityEngine;
using Zenject;
using Controllers;
using GameConfiguration;

namespace PlayableElements
{
    public class PlayerController : MonoBehaviour
    {
        [Inject] private GameConfig _gameConfig;
        [Inject] private GameController _gameController;
        [Inject] private SignalBus _signalBus;
        [Inject] private ScoreController _scoreController;
        private Rigidbody2D _rigidbody;
        private Quaternion _forwardRotation;
        private Quaternion _downRotation;

        private void Start()
        {
            _rigidbody = GetComponent<Rigidbody2D>();
            _rigidbody.simulated = false;
            ResetPlayer();

            _signalBus.Subscribe<OnStartSignal>(ResetPlayer);
            _signalBus.Subscribe<OnPlaySignal>(OnGameStart);
            _signalBus.Subscribe<OnWaitSignal>(OnPause);
        }

        private void Update()
        {
            Move();

            OnPause();
        }

        private void Move()
        {
#if UNITY_STANDALONE_WIN
        if (Input.GetKeyDown(KeyCode.Space) && _gameController.gameState == GameState.Play)
#endif
#if UNITY_ANDROID
            if (Input.GetMouseButtonDown(0) && _gameController.GameState == GameState.Play)
#endif
            {
                transform.rotation = _forwardRotation;
                _rigidbody.velocity = Vector3.zero;
                _rigidbody.AddForce(Vector2.up * _gameConfig.tapForce, ForceMode2D.Force);
            }

            transform.rotation = Quaternion.Lerp(transform.rotation, _downRotation, Time.deltaTime);
        }

        private void OnGameStart()
        {
            _forwardRotation = Quaternion.Euler(0, 0, 15);
            _downRotation = Quaternion.Euler(0, 0, -90);
            _rigidbody.velocity = Vector3.zero;
            _rigidbody.simulated = true;
        }

        private void OnPause()
        {
            if (_gameController.GameState == GameState.Wait)
            {
                _rigidbody.simulated = false;
                _rigidbody.velocity = Vector3.zero;
                transform.rotation = Quaternion.Euler(Vector3.forward);
            }
            else if (_gameController.GameState == GameState.Play) _rigidbody.simulated = true;
        }

        private void ResetPlayer()
        {
            transform.rotation = Quaternion.Euler(Vector3.zero);
            _rigidbody.simulated = false;
            _rigidbody.velocity = Vector3.zero;
            transform.position = _gameConfig.birdStartPos;
        }

        private void OnDeath()
        {
            _gameController.GameStateChange(GameState.Death);
            _rigidbody.simulated = false;
        }

        private void OnScoreZone()
        {
            _scoreController.IncreaseScore();
        }

        public void OnTriggerEnter2D(Collider2D collision)
        {
            if (collision.gameObject.tag == "Score Zone") OnScoreZone();

            if (collision.gameObject.tag == "Death Zone")
            {
                OnDeath();
            }
        }
    }
}

