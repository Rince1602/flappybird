﻿using UnityEngine;
using Zenject;
using UnityEngine.UI;
using Controllers;
using GameConfiguration;

namespace UI
{
    public class PlayerUI : MonoBehaviour
    {
        [Inject] private SignalBus _signalBus;
        [Inject] private ScoreController _scoreController;
        [Inject] private GameController _gameController;
        [SerializeField] private GameObject _playerUIPanel;
        [SerializeField] private GameObject _pausePanel;
        [SerializeField] private Text _currentScore;

        private void Start()
        {
            _signalBus.Subscribe<OnScoreChangeSignal>(UpdateScore);
        }

        public void PauseButton()
        {
            _gameController.GameStateChange(GameState.Wait);
            _playerUIPanel.GetComponent<Image>().color = new Color(0, 0, 0, 0.5f);
            _pausePanel.SetActive(true);
        }

        public void ContinueButton()
        {
            _gameController.GameStateChange(GameState.Play);
            _playerUIPanel.GetComponent<Image>().color = new Color(0, 0, 0, 0);
            _pausePanel.SetActive(false);
        }

        public void MenuButtonPlayerUi()
        {
            _playerUIPanel.GetComponent<Image>().color = new Color(0, 0, 0, 0);
            _gameController.GameStateChange(GameState.Start);
            _pausePanel.SetActive(false);
        }

        private void UpdateScore()
        {
            _currentScore.text = _scoreController.Score.ToString();
        }
    }
}
