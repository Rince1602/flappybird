﻿using UnityEngine;
using Zenject;
using GameConfiguration;

namespace Controllers
{
    public class ScoreController : IInitializable
    {
        private SignalBus _signalBus;

        public ScoreController(SignalBus signalBus)
        {
            _signalBus = signalBus;
        }

        public int Score { get; private set; }

        public void Initialize()
        {
            _signalBus.Subscribe<OnDeathSignal>(SetHighScore);
        }

        public void IncreaseScore()
        {
            Score++;
            _signalBus.Fire<OnScoreChangeSignal>();
        }

        private void SetHighScore()
        {
            if (PlayerPrefs.HasKey("HighScore"))
            {
                if (PlayerPrefs.GetInt("HighScore") < Score) PlayerPrefs.SetInt("HighScore", Score);
            }
            else
            {
                PlayerPrefs.SetInt("HighScore", Score);
            }
        }
    }
}
