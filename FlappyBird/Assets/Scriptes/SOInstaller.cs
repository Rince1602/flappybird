﻿using UnityEngine;
using Zenject;
using Controllers;

namespace GameConfiguration
{
    [CreateAssetMenu(fileName = "SO Installer", menuName = "Create SO Installer")]
    public class SOInstaller : ScriptableObjectInstaller
    {
        [SerializeField] private GameConfig _gameConfig;

        public override void InstallBindings()
        {
            SignalBusInstaller.Install(Container);

            Container.DeclareSignal<OnPlaySignal>();
            Container.DeclareSignal<OnStartSignal>();
            Container.DeclareSignal<OnWaitSignal>();
            Container.DeclareSignal<OnDeathSignal>();
            Container.DeclareSignal<OnScoreChangeSignal>();

            Container.BindInstance(_gameConfig);
            Container.Bind<GameController>().AsSingle().NonLazy();
            Container.Bind<ScoreController>().AsSingle().NonLazy();
        }
    }
}

