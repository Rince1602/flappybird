﻿using UnityEngine;
using Zenject;
using UI;
using GameConfiguration;

namespace Controllers
{
    public class UIController : MonoBehaviour
    {
        [Inject] private SignalBus _signalBus;
        [Inject] private StartMenuUI _startMenu;
        [Inject] private PlayerUI _playerUi;
        [Inject] private DeathCanvasUI _deathCanvas;
        [Inject] private GameController _gameController;

        private void Start()
        {
            _signalBus.Subscribe<OnStartSignal>(OnStart);
            _signalBus.Subscribe<OnPlaySignal>(OnPlay);
            _signalBus.Subscribe<OnDeathSignal>(OnDeath);
        }

        private void Update()
        {
            if (_gameController.GameState == GameState.Start)
            {
                _startMenu.gameObject.SetActive(true);
                _deathCanvas.gameObject.SetActive(false);
                _playerUi.gameObject.SetActive(false);
            }
        }

        private void OnStart()
        {
            _startMenu.gameObject.SetActive(true);
            _playerUi.gameObject.SetActive(false);
            _deathCanvas.gameObject.SetActive(false);
        }

        private void OnPlay()
        {
            _playerUi.gameObject.SetActive(true);
            _startMenu.gameObject.SetActive(false);
        }

        private void OnDeath()
        {
            _playerUi.gameObject.SetActive(false);
            _deathCanvas.gameObject.SetActive(true);
            _deathCanvas.ActiveDeathPanel();
        }
    }
}
