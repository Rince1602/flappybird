﻿using UnityEngine;
using Zenject;
using UnityEngine.UI;
using Controllers;

namespace UI
{
    public class StartMenuUI : MonoBehaviour
    {
        [Inject] private GameController _gameController;
        [SerializeField] private GameObject _startMenuPanel;
        [SerializeField] private GameObject _settingsPanel;
        [SerializeField] private GameObject _scorePanel;
        [SerializeField] private Text _highScoreText;
        [SerializeField] private Text _soundText;
        private int _isSound = 1;

        void Start()
        {
            if (PlayerPrefs.HasKey("Sound"))
            {
                _isSound = PlayerPrefs.GetInt("Sound");
                _soundText.text = PlayerPrefs.GetString("SoundText");
            }
        }

        public void StartButton()
        {
            _gameController.GameStateChange(GameState.Play);
        }

        public void SettingsButton()
        {
            _settingsPanel.SetActive(true);
        }

        public void SoundButton()
        {
            if (_isSound == 1)
            {
                _soundText.text = "Sound: OFF";
                _isSound = 0;
                PlayerPrefs.SetInt("Sound", _isSound);
                PlayerPrefs.SetString("SoundText", _soundText.text);
            }
            else if (_isSound == 0)
            {
                _soundText.text = "Sound: ON";
                _isSound = 1;
                PlayerPrefs.SetInt("Sound", _isSound);
                PlayerPrefs.SetString("SoundText", _soundText.text);
            }
        }

        public void ScoreButton()
        {
            _scorePanel.SetActive(true);
            _highScoreText.text = PlayerPrefs.GetInt("HighScore", 0).ToString();
        }

        public void CloseButton(GameObject panel)
        {
            panel.SetActive(false);
        }
    }
}