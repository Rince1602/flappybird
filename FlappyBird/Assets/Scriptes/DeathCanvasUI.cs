﻿using UnityEngine;
using Zenject;
using UnityEngine.UI;
using Controllers;

namespace UI
{
    public class DeathCanvasUI : MonoBehaviour
    {
        [Inject] private SignalBus _signalBus;
        [Inject] private GameController _gameController;
        [Inject] private ScoreController _scoreController;
        [SerializeField] private Text _deathScoreText;
        [SerializeField] private Text _deathHSText;

        public void ActiveDeathPanel()
        {
            _deathScoreText.text = _scoreController.Score.ToString();
            _deathHSText.text = PlayerPrefs.GetInt("HighScore").ToString();
        }

        public void MenuButtonDeathUi()
        {
            _gameController.GameStateChange(GameState.Start);
        }
    }
}

