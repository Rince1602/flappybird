﻿using Zenject;
using GameConfiguration;

namespace Controllers
{
    public enum GameState
    {
        Start,
        Play,
        Wait,
        Death
    }

    public class GameController : IInitializable
    {
        private SignalBus _signalBus;

        public GameController(SignalBus signalBus)
        {
            _signalBus = signalBus;
        }

        public GameState GameState { get; private set; }

        public void Initialize()
        {
            GameState = GameState.Start;
        }

        private void OnPlayerDeath()
        {
            GameStateChange(GameState.Death);
        }

        public void GameStateChange(GameState state)
        {
            GameState = state;

            switch (GameState)
            {
                case GameState.Play:
                {
                    _signalBus.Fire<OnPlaySignal>();

                    break;
                }
                case GameState.Start:
                {
                    _signalBus.Fire<OnStartSignal>();

                    break;
                }
                case GameState.Wait:
                {
                    _signalBus.Fire<OnWaitSignal>();

                    break;
                }
                case GameState.Death:
                {
                    _signalBus.Fire<OnDeathSignal>();

                    break;
                }
                default:
                {
                    break;
                }
            }
        }
    }
}