﻿using System.Collections;
using UnityEngine;
using Zenject;

namespace PlayableElements
{
    public class PipeController : MonoBehaviour
    {
        private float _speed = 2;
        private float _lifeTime = 15;

        private void FixedUpdate()
        {
            gameObject.transform.position += Vector3.left * _speed * Time.deltaTime;
        }

        public void OnSpawned()
        {
            StartCoroutine(LifeTimerCo());
        }

        private IEnumerator LifeTimerCo()
        {
            yield return new WaitForSeconds(_lifeTime);
            gameObject.transform.position = Vector3.zero;
            gameObject.SetActive(false);
        }
    }
}

