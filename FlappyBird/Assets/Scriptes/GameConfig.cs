﻿using UnityEngine;

namespace GameConfiguration
{
    [CreateAssetMenu(fileName = "Game Config", menuName = "Create Game Config")]
    public class GameConfig : ScriptableObject
    {
        [Header("Player Config")] public int tapForce;
        public Vector3 birdStartPos;

        [Header("Pipes Config")] public Vector3 pipesStartPos;
        public GameObject pipesPrefab;
        public int pipesCount;
        public float minYSpawn;
        public float maxYSpawn;
        public float spawnRate;
    }
}